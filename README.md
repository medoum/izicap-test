# Doumbouya-Test-IZICAP



## CHATGPT API CALLING PROJECT

The objective of this project is to set up a microservice which will be an API allowing to communicate with ChatGPT.

In my case i use Spring boot to build the API.

## HOW TO EXECUTE THIS PROJECT ?

- THE FIRST STEP IS TO :

    Build a docker image by executing this following command:

```
docker build -t test_izicap .
```

- AND THE NEXT STEP IS TO:

    Run the docker image with this following command:

```
docker run -p 8000:8090 test_izicap
```

## TOOLS

- Spring Boot
- Maven

