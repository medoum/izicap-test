package medoum.test.izicap.chatgpt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import medoum.test.izicap.FormInputDTO;
import medoum.test.izicap.OpenAiApiClient;
import medoum.test.izicap.OpenAiApiClient.OpenAiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;



// The controller

@Controller
public class ChatGptController {
	
	private static final String MAIN_PAGE = "index";
	
	@Autowired private ObjectMapper jsonMapper;
	@Autowired private OpenAiApiClient client;


	
	private String chatWithGpt3(String message) throws Exception {
		var completion = CompletionRequest.defaultWith(message);
		var postBodyJson = jsonMapper.writeValueAsString(completion);
		var responseBody = client.postToOpenAiApi(postBodyJson, OpenAiService.GPT_3);
		var completionResponse = jsonMapper.readValue(responseBody, CompletionResponse.class);
		return completionResponse.firstAnswer().orElseThrow();
	}

	// Getting the home page
	
	@GetMapping(path = "/")
	public String index() {
		return MAIN_PAGE;
	}


	// Sending the request and returning the response to the main page
	@PostMapping(path = "/")
	public String chat(Model model, @ModelAttribute FormInputDTO dto) throws FileNotFoundException {
		try {
			model.addAttribute("request", dto.prompt());
			model.addAttribute("response", chatWithGpt3(dto.prompt()));
		} catch (Exception e) {
			model.addAttribute("response", "Error in communication with OpenAI ChatGPT API.");
		}

		// Saving the request and the response in the variables

		var question1 = String.valueOf(model.getAttribute("request"));
		var response1 = String.valueOf(model.getAttribute("response"));

		// Creating the csv file and saving the request and the response in the csv file

		String csvFile = "file.csv";
		CSVWriter writer;
		try {
			writer = new CSVWriter(new FileWriter(csvFile, true));
			String[] data = {question1, response1};
			writer.writeNext(data);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return MAIN_PAGE;
	}
	
}
